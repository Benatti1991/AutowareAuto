# Copyright 2020 the Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Co-developed by Tier IV, Inc. and Apex.AI, Inc.
from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from ament_index_python import get_package_share_directory
import os


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():
    """
    Launch necessary dependencies for working with CHRONO simulator and ROS 2/Autoware.Auto.

    The CHRONO interface, which translates inputs and outputs to and from ROS standard coordinate
    systems, and the ros2 web bridge, which allows CHRONO to pick up ROS 2 topics.
    """
    # --------------------------------- Params -------------------------------

    # In combination 'raw', 'basic' and 'high_level' control
    # in what mode of control comands to operate in,
    # only one of them can be active at a time with a value
    control_command_param = DeclareLaunchArgument(
        'control_command',
        default_value="basic",  # use "raw", "basic" or "high_level" # SB: using VehicleControlCommand
        description='command control mode')

    # Default chrono_interface params
    chrono_interface_param = DeclareLaunchArgument(
        'chrono_interface_param',
        default_value=[
            get_share_file('chrono_interface', 'param/chrono.param.yaml')
        ],
        description='Path to config file for chrono interface')

    # -------------------------------- Nodes-----------------------------------

    # CHRONO interface
    chrono_interface = Node(
        package='chrono_interface',
        executable='chrono_interface_exe',
        namespace='vehicle',
        output='screen',

        parameters=[
            LaunchConfiguration('chrono_interface_param'),
            # overwrite parameters from yaml here
            {"control_command": LaunchConfiguration('control_command')}
        ],
        remappings=[
            ("vehicle_control_cmd", "/chrono/vehicle_control_cmd"), # to Chrono (controls)
            ("vehicle_state_cmd", "/chrono/vehicle_state_cmd"),     # to Chrono (target state)
            ("state_report", "/chrono/state_report"),               # from Chrono (current state)
            ("state_report_out", "state_report"),                   # TODO SB not clear
            ("gnss_odom", "/chrono/gnss_odom"),                     # TODO SB not clear
            ("vehicle_odom", "/chrono/vehicle_odom")                # from chrono, odometry msg
        ]
    )

    ld = LaunchDescription([
        control_command_param,
        chrono_interface_param,
        chrono_interface
    ])
    return ld

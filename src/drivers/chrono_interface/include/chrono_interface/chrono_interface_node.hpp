// Copyright 2020 the Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
/// \file
/// \brief Implementation of vehicle interface for CHRONO simulator

#ifndef CHRONO_INTERFACE__CHRONO_INTERFACE_NODE_HPP_
#define CHRONO_INTERFACE__CHRONO_INTERFACE_NODE_HPP_

#include <chrono_interface/visibility_control.hpp>

#include <vehicle_interface/vehicle_interface_node.hpp>

#include <chrono>
#include <string>

namespace chrono_interface
{

/// Node wrapping ChronoInterface.
class CHRONO_INTERFACE_PUBLIC ChronoInterfaceNode
  : public ::autoware::drivers::vehicle_interface::VehicleInterfaceNode
{
public:
  /// ROS 2 parameter constructor
  /// \param[in] options An rclcpp::NodeOptions object
  explicit ChronoInterfaceNode(const rclcpp::NodeOptions & options);
};  // class ChronoInterfaceNode
}  // namespace chrono_interface

#endif  // CHRONO_INTERFACE__CHRONO_INTERFACE_NODE_HPP_

# Copyright 2020, The Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch Modules for Milestone 3 of the AVP 2020 Demo."""

from ament_index_python import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

import os


def generate_launch_description():
    """
    Launch all nodes defined in the architecture for Milestone 3 of the AVP 2020 Demo.

    More details about what is included can
    be found at https://gitlab.com/autowarefoundation/autoware.auto/AutowareAuto/-/milestones/25.
    """
    avp_demo_pkg_prefix = get_package_share_directory('autoware_auto_avp_demo')
    # SB: try not to use this
    chrono_param_file = os.path.join(
        avp_demo_pkg_prefix, 'param/chrono_interface.param.yaml')
    lanelet2_osm_file_path = os.path.join(
        avp_demo_pkg_prefix, 'chrono_data/sanfrancisco.osm')
    map_publisher_param_file = os.path.join(
        avp_demo_pkg_prefix, 'param/map_publisher_chrono.param.yaml')
    ndt_localizer_param_file = os.path.join(
        avp_demo_pkg_prefix, 'param/ndt_localizer.param.yaml')
    mpc_param_file = os.path.join(
        avp_demo_pkg_prefix, 'param/mpc.param_chrono_sedan.yaml')

    pc_filter_transform_param_file = os.path.join(
        avp_demo_pkg_prefix, 'param/pc_filter_transform_chrono.param.yaml')

    urdf_pkg_prefix = get_package_share_directory('chrono_sedan_description')
    urdf_path = os.path.join(urdf_pkg_prefix, 'urdf/chrono_sedan.urdf')
    with open(urdf_path, 'r') as infp:
        urdf_file = infp.read()

    # Arguments

    chrono_interface_param = DeclareLaunchArgument(
        'chrono_interface_param_file',
        default_value=chrono_param_file,
        description='Path to config file for CHRONO Interface'
    )
    map_publisher_param = DeclareLaunchArgument(
        'map_publisher_param_file',
        default_value=map_publisher_param_file,
        description='Path to config file for Map Publisher'
    )
    ndt_localizer_param = DeclareLaunchArgument(
        'ndt_localizer_param_file',
        default_value=ndt_localizer_param_file,
        description='Path to config file for ndt localizer'
    )
    mpc_param = DeclareLaunchArgument(
        'mpc_param_file',
        default_value=mpc_param_file,
        description='Path to config file for MPC'
    )
    pc_filter_transform_param = DeclareLaunchArgument(
        'pc_filter_transform_param_file',
        default_value=pc_filter_transform_param_file,
        description='Path to config file for Point Cloud Filter/Transform Nodes'
    )

    # Nodes

    chrono_interface = Node(
        package='chrono_interface',
        executable='chrono_interface_exe',
        namespace='vehicle',
        name='chrono_interface_node',
        output='screen',
        parameters=[
          LaunchConfiguration('chrono_interface_param_file')
        ],
        remappings=[
            ("vehicle_control_cmd", "/chrono/vehicle_control_cmd"),
            ("vehicle_state_cmd", "/chrono/vehicle_state_cmd"),
            ("state_report", "/chrono/state_report"),
            ("vehicle_odom", "/chrono/vehicle_odom")
        ]
    )
    filter_transform_vlp16_front = Node(
        package='point_cloud_filter_transform_nodes',
        executable='point_cloud_filter_transform_node_exe',
        name='filter_transform_vlp16_front',
        namespace='lidar_front',
        parameters=[LaunchConfiguration('pc_filter_transform_param_file')],
        remappings=[("points_in", "points_raw")]
    )
# SB: No rear lidar
    """filter_transform_vlp16_rear = Node(
        package='point_cloud_filter_transform_nodes',
        executable='point_cloud_filter_transform_node_exe',
        name='filter_transform_vlp16_rear',
        namespace='lidar_rear',
        parameters=[LaunchConfiguration('pc_filter_transform_param_file')],
        remappings=[("points_in", "points_raw")]
    )"""
    map_publisher = Node(
        package='ndt_nodes',
        executable='ndt_map_publisher_exe',
        namespace='localization',
        # TODO SB: set san francisco map here
        parameters=[LaunchConfiguration('map_publisher_param_file')]
    )
    # SB TODO: set lanelet here
    lanelet2_map_loader = Node(
        package='lanelet2_map_provider',
        executable='lanelet2_map_provider_exe',
        namespace='had_maps',
        # TODO SB: set san francisco map here
        parameters=[{'map_osm_file': lanelet2_osm_file_path}]
    )
    urdf_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        parameters=[{'robot_description': urdf_file}],
    )
    ndt_localizer = Node(
        package='ndt_nodes',
        executable='p2d_ndt_localizer_exe',
        namespace='localization',
        name='p2d_ndt_localizer_node',
        parameters=[LaunchConfiguration('ndt_localizer_param_file')],
        remappings=[
            ("points_in", "/lidars/points_fused_downsampled"),
            ("observation_republish", "/lidars/points_fused_viz"),
        ]
    )
    mpc = Node(
        package='mpc_controller_nodes',
        executable='mpc_controller_node_exe',
        name='mpc_controller_node',
        namespace='control',
        parameters=[LaunchConfiguration('mpc_param_file')]
    )

    core_launch = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([avp_demo_pkg_prefix, '/launch/ms3_core.launch.py']),
        launch_arguments={}.items()
    )

    return LaunchDescription([
        chrono_interface_param,
        map_publisher_param,
        #ndt_localizer_param,
        mpc_param,
        pc_filter_transform_param,
        urdf_publisher,
        chrono_interface,
        map_publisher,
        # SB: do we need this?
        ndt_localizer,
        mpc,
        filter_transform_vlp16_front,
        #filter_transform_vlp16_rear,
        core_launch,
    ])
